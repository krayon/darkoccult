# How to Print

There are two ways to print this game: automatic or manual. Printing
with automation is easier if you know what you are doing. Printing
manually means you have to assemble everything yourself, so it takes
longer, but you can probably stumble through it without knowing what
you are doing.


## Printing Manually

1. Download and install [Inkscape](http://inkscape.org)

2. Open 9 cards in Inkscape and layout evenly on a page

3. Print

Rinse and repeat...


## Auto Printing

This card deck was made entirely with open source software on an open
source operating system, so the only way I have tested an automated
build is on Linux and BSD.

Linux is free to use, so if you're up for it, just boot Linux, build,
and print. If you don't want to boot into Linux or BSD to build the
deck but are handy with scripting on your platform, feel free to
modify my build script for your own use.

Basically, there are three steps: get the software needed to build,
run the build script, and print.

In that order:

1. Get the Software

   * [Linux](https://linux.com/learn/5-live-linux-desktop-distributions-you-should-know)
   * [bash shell](https://gnu.org/software/bash)
   * [pdfjam](http://www2.warwick.ac.uk/fac/sci/statistics/staff/academic-research/firth/software/pdfjam)
   * [ImageMagick](http://imagemagick.org/script/index.php)

   For docs as HTML:

   * [python](http://python.org)
   * [sphinx](http://www.sphinx-doc.org/en/stable)

   For doc as PDF:

   * [texlive](https://www.tug.org/texlive/)


2. Run the build script:

       $ sh ./builder.sh build .
       $ sh ./builder.sh doc .

3. Print. The printable pages are in a new directory called
   `print`. Remember to print both the front and the back!

