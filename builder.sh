#!/bin/sh
# bash script to build Dark oCCult for printing

# GNU All-Permissive License
# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.  This file is offered as-is,
# without any warranty.

ARG=$1
CWD=`pwd`
VILE="true"
DEST=${DEST:-print}
RES=${RES:-300}

help() {
    echo "builder.sh [build|docs|clean] /path/to/dir" || $VILE
    echo "build Dark oCCult for printing" || $VILE
    echo " "
    echo "builder.sh build /path/to/dark/oCCult/dir -> build card set for printing" || $VILE
    echo "builder.sh doc /path/to/dark/oCCult/dir   -> build card set for printing" || $VILE
    echo "builder.sh clean /path/to/dark/oCCult/dir -> start over" || $VILE
    echo " "
    exit
}

build() {
# do we have the fonts?
fc-list | grep -i "essays1743" || echo "Essays1743 font missing. Install from the fonts directory and try again." ; exit 1
fc-list | grep -i "orbitron" || echo "Orbitron font missing. Install from the fonts directory and try again." ; exit 1
fc-list | grep -i "ubuntu" || echo "Ubuntu font missing. Install from the fonts directory and try again." ; exit 1

#sanity checks
PDFNUP=${PDFNUP:-`which pdfnup`} || PDFNUP=""
CONVERT=${CONVERT:-`which convert`} || CONVERT=${CONVERT:-`which gm` convert}
MONTAGE=${MONTAGE:-`which montage`} || MONTAGE=${MONTAGE:-`which gm` montage}

if [ X"$CONVERT" = "X" -o X"$MONTAGE" = "X" ]; then
    echo "ImageMagick or GraphicsMagick is required, but cannot be found."
    exit 1
fi

# convert svg to png
mkdir "${ARG}"/"${DEST}"
cat "${ARG}"/deck/system/back.png > "${ARG}"/"${DEST}"/back.png
find "${ARG}" -type f -iname "*.svg" | xargs "${CONVERT}" {} -density $RES -rotate 90 "${1}"/"${DEST}"/{}.png
echo "Rasterisation complete." || $VILE

# this is where the pngs live
cd "${ARG}"/"${DEST}"
mkdir "${ARG}"/"${DEST}"/_trash

if [ X"$PDFNUP" = "X" ]; then
    montage -density 300 -page A4 `ls -1 *.svg.png | head -n9` deck-`date +%s | cut -b 8-`.pdf && mv `ls -1 *.svg.png | head -n9` "${ARG}"/"${DEST}"/_trash/
    montage -density 300 back.png back.png back.png \
		back.png back.png back.png \
		back.png back.png back.png -page A4 back.pdf
else:
    "${PDFNUP}" --nup 3x3 --suffix '3x3' `ls -1 *.svg.png | head -n9` && mv `ls -1 *.svg.png | head -n9` "${ARG}"/"${DEST}"/_trash/
    "${PDFNUP}" --nup 3x3 --suffix '3x3'   \
		back.png back.png back.png \
		back.png back.png back.png \
		back.png back.png back.png

    echo "Print-spreads generated as single signature documents in the print folder." || $VILE
fi
}

crop() {
    #which mogrify are we using
    MOGRIFY=${MOGRIFY:-`which mogrify`} || MOGRIFY=${MOGRIFY:-`which gm` mogrify}

   find "${ARG}"/"${DEST}" -type f -name "*.pdf" | xargs "${MOGRIFY}" -density $RES -draw 'lines  740,0 740,20'
   find "${ARG}"/"${DEST}" -type f -name "*.pdf" | xargs "${MOGRIFY}" -density $RES -draw 'lines 1480,0 1480,20'
   find "${ARG}"/"${DEST}" -type f -name "*.pdf" | xargs "${MOGRIFY}" -density $RES -draw 'lines 0,1047 20,1047'
   find "${ARG}"/"${DEST}" -type f -name "*.pdf" | xargs "${MOGRIFY}" -density $RES -draw 'lines 0,2094 20,2094'
}

doc() {
    cd "${ARG}"/doc
    make html
    make pdf || echo "In doc build: PDF failed, but HTML version succeeded and is printable." || $VILE
    mkdir "${ARG}"/"${DEST}" 2>/dev/null || true
    mv "${ARG}"/doc/build "${ARG}"/"${DEST}"/docs
    echo "A fitful slumber brings the nights adventures to an end." || $VILE
}

clean() {
    /bin/rm -r "${ARG}"/"${DEST}"
    echo "Everything is clean, and tidy, and back to normal." || $VILE
}

## parse
while [ True ]; do
if [ "$1" = "clean" -o "$1" = "--clean" ]; then
    CLEAN=1
    shift 1
elif  [ "$1" = "doc" -o "$1" = "--doc" -o "$1" = "docs" -o "$1" = "--docs" ]; then 
    DOC=1
    shift 1
elif  [ "$1" = "build" -o "$1" = "--build" -o "$1" = "bld" -o "$1" = "-b" ]; then 
    BUILD=1
    shift 1
elif  [ "$1" = "crop" -o "$1" = "--crop" ]; then 
    CROP=1
    shift 1
elif  [ "$1" = "evil" -o "$1" = "--vile" -o "$1" = "666" -o "$1" = "redrum" ]; then 
    EVIL=1
    shift 1
elif  [ "$1" = "help" -o "$1" = "--help" -o "$1" = "-h" -o "$1" = "--options" -o "$1" = "options" -o "$1" = "" ]; then 
    help
    shift 1
else
    break
fi
done

if [ X"${1}" = "X" ]; then
    ARG="."
fi

## main
if [ X"$CLEAN" = X"1" ]; then
    # clean first
    clean "${ARG}"
fi

if [ X"$EVIL" = X"1" ]; then
    # find out if we're evil
    echo "Something beckons you from the comfort of your home, out into the night..."
    VILE='tr "[A-Za-z]" "[N-ZA-Mn-za-m]"'
fi 

if [ X"$BUILD" = X"1" ]; then
    #then build the cards
    build "${ARG}"
fi

if [ X"$CROP" = X"1" ]; then
    #then draw crop lines
    crop "${ARG}"
fi

if [ X"$DOC" = X"1" ]; then
    #and the docs
    doc "${ARG}"
fi

